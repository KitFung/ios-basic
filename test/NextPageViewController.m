//
//  NextPageViewController.m
//  test
//
//  Created by Kit Fung on 25/8/15.
//  Copyright (c) 2015 Kit Fung. All rights reserved.
//

#import "NextPageViewController.h"

@interface NextPageViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *imageView;


@end

@implementation NextPageViewController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.imageView.image = self.image;
    self.imageView.contentMode = UIViewContentModeScaleAspectFit;
}

- (void) viewDidLoad{
    [super viewDidLoad];
   // UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:@"Back" style: UIBarButtonItemStylePlain target:self action:@selector(Back)];
   // self.navigationItem.leftBarButtonItem = backButton;
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
//    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
//    // Override point for customization after application launch.
//    self.viewController = [[ViewController alloc] initWithNibName:@"ViewController"
//                                                           bundle:nil];
//    UINavigationController *navigation = [[UINavigationController alloc]initWithRootViewController:self.viewController];
//    self.window.rootViewController = navigation;
//    [self.window makeKeyAndVisible];
//    return YES;
    return YES;
}

@end
