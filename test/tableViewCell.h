//
//  TableViewCell.h
//  test
//
//  Created by Kit Fung on 24/8/15.
//  Copyright (c) 2015 Kit Fung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TableViewCell : UITableViewCell
@property (nonatomic,weak) IBOutlet UIButton *leftButton;
@property (nonatomic,weak) IBOutlet UISwitch *rightSwitch;
@property (weak, nonatomic) IBOutlet UILabel *textLabel;
@end
