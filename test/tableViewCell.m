//
//  TableViewCell.m
//  test
//
//  Created by Kit Fung on 24/8/15.
//  Copyright (c) 2015 Kit Fung. All rights reserved.
//

#import "TableViewCell.h"

@interface TableViewCell ()
@end

@implementation TableViewCell

@synthesize leftButton = _leftButton;
@synthesize rightSwitch = _rightSwitch;
@synthesize textLabel = _textLabel;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
