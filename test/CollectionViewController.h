//
//  CollectionViewController.h
//  test
//
//  Created by Kit Fung on 25/8/15.
//  Copyright (c) 2015 Kit Fung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CollectionViewController : UICollectionViewController

@end
