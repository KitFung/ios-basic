//
//  CollectionViewCell.h
//  test
//
//  Created by Kit Fung on 25/8/15.
//  Copyright (c) 2015 Kit Fung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgView;
@property (strong, nonatomic) IBOutlet UIButton *dumpBottom;
@end
