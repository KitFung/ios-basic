//
//  swipeViewController.h
//  test
//
//  Created by Kit Fung on 24/8/15.
//  Copyright (c) 2015 Kit Fung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface swipeViewController : UIViewController <UITableViewDelegate, UITableViewDataSource,UISearchResultsUpdating>

@end
