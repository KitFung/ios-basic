//
//  AddDropController.m
//  test
//
//  Created by Kit Fung on 21/8/15.
//  Copyright (c) 2015 Kit Fung. All rights reserved.
//

#import "AddDropController.h"
@interface AddDropController()

@property (weak, nonatomic) IBOutlet UIStepper *stepper;
@property (weak, nonatomic) IBOutlet UITextField *textFF;
@property (weak, nonatomic) IBOutlet UITextView *pool;

@property (strong) NSArray *list;
@property NSInteger count;
@property NSInteger maxssss;
@property NSInteger interval;
@end


@implementation AddDropController

-(void)viewDidLoad{
    [super viewDidLoad];
    UIScrollView *scrollView = [[UIScrollView alloc] initWithFrame:self.view.bounds];
    [self.view addSubview:scrollView];
    [scrollView setContentSize:CGSizeMake(scrollView.bounds.size.width, scrollView.bounds.size.height*3)];
    
    [self.stepper addTarget:self action:@selector(stepperValueIschanged) forControlEvents:UIControlEventValueChanged];
    [self.stepper setMinimumValue:1];
    [self.stepper setMaximumValue:10];
    [self.stepper setTintColor:[UIColor colorWithRed:0.000 green:0.000 blue:0.630 alpha:1.000]];
    [self stepperValueIschanged];
    
    self.interval = 1;
    self.list =[[NSArray alloc] init];
    self.list = [self.pool.text componentsSeparatedByString:@" "];
    self.count = [self.list count];
    self.maxssss = self.count;
    
}

- (IBAction)addButtom:(id)sender {
    NSInteger tmp = MIN(self.maxssss, self.count+self.interval);
    if (self.count < self.maxssss ) {
        self.count = tmp;
        self.pool.text = [[self.list subarrayWithRange:NSMakeRange(0, self.count)] componentsJoinedByString:@" "];
    }
}
- (IBAction)MinusButton:(id)sender {
    NSInteger tmp = MAX(0, self.count - self.interval);
    if (self.count > 0) {
        self.count = tmp;
        self.pool.text = [[self.list subarrayWithRange:NSMakeRange(0, self.count)] componentsJoinedByString:@" "];
    }
}

- (void)stepperValueIschanged {
    self.interval = self.stepper.value;
    [self.textFF setText:[NSString stringWithFormat:@"%.1f", self.stepper.value]];
}

@end
