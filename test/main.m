//
//  main.m
//  test
//
//  Created by Kit Fung on 21/8/15.
//  Copyright (c) 2015 Kit Fung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
