//
//  ScrollingController.h
//  test
//
//  Created by Kit Fung on 21/8/15.
//  Copyright (c) 2015 Kit Fung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ScrollingController : UITableViewController <UITableViewDelegate, UITableViewDataSource,UIGestureRecognizerDelegate>
@end
