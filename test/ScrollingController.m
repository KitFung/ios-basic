//
//  ScrollingController.m
//  test
//
//  Created by Kit Fung on 21/8/15.
//  Copyright (c) 2015 Kit Fung. All rights reserved.
//

#import "ScrollingController.h"
#import "TableViewCell.h"

@interface ScrollingController ()
@property (strong, nonatomic) IBOutlet UITableView *tableViews;
@property (nonatomic,strong) NSArray *data;
@property (nonatomic,strong) UILongPressGestureRecognizer *longRecongizer;
@end

@implementation ScrollingController
static NSString *cellIdentifier;
-(void)viewDidLoad{
    
    [super viewDidLoad];
    self.longRecongizer = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPressHandle:)];
    // self.longRecongizer.numberOfTapsRequired = 1;
    // self.longRecongizer.minimumPressDuration = 1;
    [self.view addGestureRecognizer:self.longRecongizer];
    
    self.data = @[@"Apple",@"Banana",@"Peach",@"Grape",@"Strawberry",@"Watermelon"];
    
    cellIdentifier = @"Cell";
    [self.tableView registerNib: [UINib nibWithNibName: @"TableViewCell" bundle: nil] forCellReuseIdentifier: cellIdentifier];
}

- (void) longPressHandle:(UILongPressGestureRecognizer*)sender{
    if (sender.state == UIGestureRecognizerStateEnded) {
        NSLog(@"Long press Ended");
    } else if (sender.state == UIGestureRecognizerStateBegan) {
       [self performSegueWithIdentifier: @"tonav" sender: self];
    }

}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.data count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    TableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath: indexPath];
    if (cell == nil) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"TableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }

    NSString* content = [self.data objectAtIndex:indexPath.row];
    cell.textLabel.text = content;
    return cell;

}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 178;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

    UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"Hungry?" message:[NSString stringWithFormat:@"I'm hungry, I think I'll eat some %@",[self.data objectAtIndex:indexPath.row]] delegate:self cancelButtonTitle:@"guilmo.com" otherButtonTitles:nil];
    [message show];
}

@end
