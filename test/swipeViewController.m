//
//  swipeViewController.m
//  test
//
//  Created by Kit Fung on 24/8/15.
//  Copyright (c) 2015 Kit Fung. All rights reserved.
//

#import "swipeViewController.h"

@interface swipeViewController () 
@property (weak, nonatomic) IBOutlet UISegmentedControl *makeChoice;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property(strong, nonatomic) UISearchController *searchController;
@property (nonatomic) NSMutableArray * list1;
@property (nonatomic) NSMutableArray * list2;
@property (nonatomic) NSMutableArray * filteredTableData;
@end
static BOOL flag = true;

@implementation swipeViewController
static NSString *cellIdentifier;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    cellIdentifier = @"lisss";
    self.list1 = [[NSMutableArray alloc] initWithObjects:@"qqq",@"ewr",@"qwqwq",@"123", nil];
    self.list2 = [[NSMutableArray alloc] initWithObjects:@"a",@"qqqqqqqq",@"12344566",@"popololo",@"lolop",@"kolp", nil];
    [self.makeChoice addTarget:self
                         action:@selector(segmentChanged)
               forControlEvents:UIControlEventValueChanged];
    self.searchController = [[UISearchController alloc] initWithSearchResultsController:nil];
    self.searchController.searchResultsUpdater = self;
    self.searchController.dimsBackgroundDuringPresentation = false;
    [self.searchController.searchBar sizeToFit];
    self.tableView.tableHeaderView = self.searchController.searchBar;
    
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:cellIdentifier];
}

- (id) smallCurrent{
    return flag?self.list1:self.list2;
}

- (id) current:(NSIndexPath *)indexPath{
    return [[self smallCurrent] objectAtIndex:indexPath.row];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)segmentChanged {
    [super didReceiveMemoryWarning];
    flag = !flag;
    [self updateSearchResultsForSearchController:self.searchController];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (!self.searchController.active) {
        NSMutableArray *currarys = [self smallCurrent];
        return [currarys count];
    }else{
        return self.filteredTableData.count;
    }
}

- (BOOL)searchBarShouldBeginEditing:(UISearchBar*)searchBar {
    NSLog(@"here");
    return YES;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath: indexPath];
    NSString* content = self.searchController.active?[self.filteredTableData objectAtIndex:indexPath.row]:[self current:indexPath];
    cell.textLabel.text = content;
    return cell;
    
}

- (void)updateSearchResultsForSearchController:(UISearchController *)searchController {
    [self.filteredTableData removeAllObjects];
    NSPredicate *searchPredicate = [NSPredicate predicateWithFormat:@"SELF CONTAINS[c] %@", self.searchController.searchBar.text];
    if (self.searchController.searchBar.text.length == 0)
        self.filteredTableData = [[self smallCurrent] mutableCopy];
    else
        self.filteredTableData = [[[self smallCurrent] filteredArrayUsingPredicate:searchPredicate] mutableCopy];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.tableView reloadData];
    });
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell     forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if ([tableView respondsToSelector:@selector(setSeparatorInset:)])
    {
        [tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([tableView respondsToSelector:@selector(setLayoutMargins:)])
    {
        [tableView setLayoutMargins:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)])
    {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"Hungry?" message:@"hahaha" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [message show];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    if (self.searchController.active) {
        self.searchController.active = NO;
        [self.searchController.searchBar removeFromSuperview];
    }
}

@end
